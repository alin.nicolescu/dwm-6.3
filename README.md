# dwm - dynamic window manager

Version `6.3` for `FreeBSD` with the following patches applied:

- `pertag`
- `noborder floating fix`
- `always center`
- `hide vacant tags`

